# JSPushNotification

> A JavaScript library to send push notifications to the browser

## How To Use

> Paste the contents of pushnotification.js/below script into your browser's developer console

```js
function pushAPI() {
  if (!Notification) {
    alert('{ Code Is Poetry }'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();

  var notification = new Notification('', {
    icon: '', //put an image source here
    body: '', //enter your message
  });

  notification.onclick = function () {
    window.open('https://gitlab.com/ArpitKharbanda'); // offcourse change it to your own URL  
  };
}
```

## License

_GNU AGPLv3_